module gitlab.com/PatrickDomnick/steam-exporter

go 1.16

require (
	github.com/prometheus/client_golang v1.5.1
	github.com/sirupsen/logrus v1.8.1
)
